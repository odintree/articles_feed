import React, { Component } from 'react';
import axios from 'axios';

import { withRouter } from "react-router-dom";




class Search extends Component {

	state = {
		query: '',
	}

	handleInputChange = () => {
		this.setState({
			query: this.search.value
		})
	}

	onClickSearch = event => {
		console.log('CLICK')
		this.getSearch(this.state.query);
	};

	getSearch = (query) => {
		console.log('sss')
		axios.get(`http://localhost:3004/data?q=${encodeURI(query)}`).then(response => {
			const articles = response.data;
			console.log('SEARCH', articles);
			this.props.history.push({
				pathname: '/result',
				state: { articles: articles }
			})
		})

	}


	render() {
		return (
			<div className="search-container">
				<form>
					<input
						placeholder="Search for..."
						ref={input => this.search = input}
						onChange={this.handleInputChange}
					/>
				</form>
				<button onClick={this.onClickSearch}>Search</button>

			</div>)
	}
}

export default withRouter(Search);