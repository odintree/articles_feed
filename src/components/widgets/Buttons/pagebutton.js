import React from 'react';
import { Link} from 'react-router-dom';


const buttons = (props) => {
    let template = null;

    switch(props.type){
        case 'loadmore':
            template = (
                <div
                    onClick={props.loadMore}
                >
                    {props.content}
                </div>
            );
            break;
        case 'linkTo':
            template = (
                <Link to={props.linkTo}>
                    {props.content}
                </Link>
            )
            break;
        default:
            template = null
    }
    return template;
}

export default buttons;