import React from 'react';
import ReactDOM from 'react-dom';
import DOMPurify from 'dompurify'





const Articles = (props) => {
	let text = ''
	let authors = ''

	switch (props.type) {
		case ('featured'):
			text = props.body.replace(/src="/g, 'src="http://cdn-assets.ziniopro.com')
			authors = props.authors
			break;
		default:
			text = '';
	}



	return (
		<div>
			<div className="content" dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(text) }}>
			</div>
			{<div className="author"> <p> Author: {authors || 'Unknown author'} </p> </div>}
		</div>
	)
}



export default Articles;