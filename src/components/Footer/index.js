import React from 'react';
import ReactDOM from 'react-dom';
import PageButton from '../widgets/Buttons/pagebutton';





const Footer = (props) => {


	const previousButton = (page) => {
		const previousPage = page * 1 - 1;
		return page != 1 ? <PageButton linkTo={`/posts/${previousPage}`} content={'Previous'} type="linkTo" /> : null
	}

	const nextButton = (page, pages) => {
		const nextPage = page * 1 + 1;
		return page != pages ? <PageButton linkTo={`/posts/${nextPage}`} content={'Next'} type="linkTo" /> : null
	}

	return (
		<footer>
			<ul>
				<li>
					{previousButton(props.page)}
				</li>

				<li>
					{nextButton(props.page, props.pages)}

				</li>
			</ul>

		</footer>
	)


}



export default Footer;