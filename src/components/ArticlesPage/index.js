import React, { Component } from 'react';
import axios from 'axios';


import Articles from '../Articles/';
import PageLayout from '../../hoc/PageLayout/layout';




class ArticlesPage extends Component {
	page = this.props.match.params.id;

	state = {
		articles: [],
		title: '',
		article: [],
		pages: 0,
		body: '',
		authors: '',
	}

	getData = (page) => {
		axios.get(`http://localhost:3004/data`).then(response => {
			const articles = response.data;
			axios.get(`http://localhost:3004/data?_start=${page - 1}&_end=${page}`)
				.then(response2 => {
					const article = response2.data[0];
					this.setState({
						articles,
						title: article.title,
						pages: articles.length,
						body: article.body,
						authors: article.authors
					})
				})
		})
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.match.params.id != this.page) {
			this.page = nextProps.match.params.id;
			this.getData(this.page);
		}

	}

	componentWillMount() {
		this.getData(this.page);

	}
	render() {
		return (
			<PageLayout title={this.state.title} page={this.page} pages={this.state.pages} type="featured">
				<Articles body={this.state.body} authors={this.state.authors} type='featured' />.
			</PageLayout>

		)
	}
}

export default ArticlesPage;