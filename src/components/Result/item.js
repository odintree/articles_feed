import React, { Component } from 'react';
import Articles from '../Articles';




const Item = (props) => {

	return (
		<div>
			<h3> {props.title} </h3>
			<Articles body={props.body} authors={props.authors} type='featured' />
		</div>
	)
}



export default Item;