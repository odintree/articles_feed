import React, { Component } from 'react';
import Item from './item';




class Result extends Component {

	state = {
		article: {},
		readMore: false
	}

	onClickReadMore(article) {
		this.setState({
			article,
			readMore: true
		})
	}

	onClickClose() {
		this.setState({
			readMore: false, 
			article: {}
		})
	}



	template = () => {
		return this.state.readMore ?
			<div>
				<a onClick={() => { this.onClickClose() }}> Close X </a>
				<Item body={this.state.article.body}
					authors={this.state.article.authors}
					title={this.state.article.title} sendData={this.getData} />
			</div>
			:
			<div>
				{this.props.location.state.articles.map((item, index) => (
					<div className="resultList">
						{item.title}  <a onClick={() => { this.onClickReadMore(item) }}> Read more </a>
					</div>
				))}
			</div>


	}

	render() {
		return (
			<div>
				{this.template()}

			</div>
		)
	}

}



export default Result;

