import React from 'react';
import ReactDOM from 'react-dom';
import Search from '../Search';



const Header = (props) => {
	
    return (
        <header>
            <ul>
                <li><a href="#"><i className="fa fa-home left"></i><span><b>TITLE:</b> {props.title}</span></a></li>

                <li><a href="#"><i className="fa fa-rss right"></i> <Search/> </a></li>
            </ul>
            <div>
            </div>
        </header>
    )
}



export default Header;