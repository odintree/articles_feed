import React from 'react';
import './layout.css'

import Header from '../../components/Header';
import Footer from '../../components/Footer';

const PageLayout = (props) => {
	let title = '';
	let page = 0;
	let pages = 0;
	switch (props.type) {
		case ('featured'):
			title = props.title; 
			page = props.page; 
			pages = props.pages;
			break;
		default:	
			break; 
	}

	return (
		<div>
			<Header title={title}/>

			{props.children}
			
			<Footer page={page} pages={pages}/>

		</div>
	)

}

export default PageLayout;