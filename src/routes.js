import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Home from './components/Home';
import Result from './components/Result';
import ArticlesPage from './components/ArticlesPage';



class Routes extends Component {
	render() {
		return (
			<Switch>
				<Route path="/" exact component={Home}/>
				<Route path="/posts/:id" exact component={ArticlesPage} />
				<Route path="/result" exact component={Result} />
			</Switch>

		)
	}
}

export default Routes;