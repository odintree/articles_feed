# Front end Articles feed

> My front end task
## Build Setup

The application base on React. 
# tools  
* React
* Node
* json-server
* webpack



``` bash
# install dependencies
npm install -g json-server

# after this to run a json-server. Make sure the port 3004 is free.
npm run db
# the json server that contains articles.json data is running 
# now you can run the app (it is going to run on port 8080)
npm run dev

```
<http://localhost:8080>

You can check my progress through commits.  



The task was interesting. Thank You for it.  
Kind regards.
